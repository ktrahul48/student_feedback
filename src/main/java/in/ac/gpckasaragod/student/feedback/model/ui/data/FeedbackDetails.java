/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.feedback.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class FeedbackDetails {
    
    
    private Integer id;
    private String SubjectName;
    private String Feedback;

    public FeedbackDetails(Integer id, String SubjectName, String Feedback) {
        this.id = id;
        this.SubjectName = SubjectName;
        this.Feedback = Feedback;
    }
    private static final Logger LOG = Logger.getLogger(FeedbackDetails.class.getName());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String SubjectName) {
        this.SubjectName = SubjectName;
    }

    public String getFeedback() {
        return Feedback;
    }

    public void setFeedback(String Feedback) {
        this.Feedback = Feedback;
    }
    
    
}
