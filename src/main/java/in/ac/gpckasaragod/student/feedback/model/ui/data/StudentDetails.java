/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.feedback.model.ui.data;

/**
 *
 * @author student
 */
public class StudentDetails {
         
    private Integer id;
    private String AdmisionNo;
    private String Name;
    private String Department;

    public StudentDetails(Integer id, String AdmisionNo, String Name, String Department) {
        this.id = id;
        this.AdmisionNo = AdmisionNo;
        this.Name = Name;
        this.Department= Department;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdmisionNo() {
        return AdmisionNo;
    }

    public void setAdmisionNo(String AdmisionNo) {
        this.AdmisionNo = AdmisionNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    public String getDepartment() {
        return Department;
    }
    
    public void setDepartment(String Department) {
        this.Department = Department;
    }        
    
    
}

    

