/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.feedback.service;

import in.ac.gpckasaragod.student.feedback.ui.FeedbackDetails;
import in.ac.gpckasaragod.student.feedback.ui.StudentDetails;
import java.awt.List;

/**
 *
 * @author student
 */

    interface FeedbackDetailsService {
    public String saveFeedbackDetails(Integer Id, String SubjectName, String Feedback);
    public FeedbackDetails readFeedbackDetails(Integer Id);
    public java.util.List<FeedbackDetails> getAllFeedbackDetails();
    public String updateFeedbackDetails(Integer id,String SubjectName,Integer Feedback);
    public String deleteFeedbackDetails(Integer id);
}
    

