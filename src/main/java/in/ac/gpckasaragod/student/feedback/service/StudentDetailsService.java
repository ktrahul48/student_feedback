/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.feedback.service;

import in.ac.gpckasaragod.student.feedback.ui.FeedbackDetails;
import in.ac.gpckasaragod.student.feedback.ui.StudentDetails;
import java.util.List;

/**
 *
 * @author student
 */
    interface StudentDetailsService {
    public String saveStudentDetails(Integer Id, String AdmisionNo, String Name, String Department);
    public StudentDetails readStudentDetails(Integer Id);
    public List<StudentDetails> getAllStudentDetails();
    public String updateStudentDetails(Integer id,String AdmisionNo, String Name, String Department );
    public String deleteStudentDetails(Integer id);
}
    

