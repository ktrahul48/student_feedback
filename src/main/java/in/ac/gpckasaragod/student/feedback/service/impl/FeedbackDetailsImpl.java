/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.feedback.service.impl;

import in.ac.gpckasaragod.student.feedback.ui.FeedbackDetails;
import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
    public class FeedbackDetailsImpl extends ConnectionServiceimpl implements Feedbackservice {
    public String saveFeedbackDetails(Integer Id, String SubjectName, String Feedback) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO FEEDBACK_DETAILS (ID,SUBJECT_NAME,FEEDBACK) VALUES " 
                   + "('"+SubjectName+ "',"+Feedback+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(FeedbackDetails.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    public FeedbackDetails readFeedbackDetails(Integer Id){
       FeedbackDetails FeedbackDetails= null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM FEEDBACK_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String SubjectName = resultSet.getString("SUBJECT_NAME");
                String Feedback = resultSet.getString("FEEDBACK");
                FeedbackDetails feedbackDetails = new FeedbackDetails(id,SubjectName,Feedback);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(FeedbackDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return FeedbackDetails;
         
    }
    
    
   
     public java.util.List<FeedbackDetails> getAllFeedbackDetails(){
        java.util.List<FeedbackDetails> FeedbackDetails= new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM FEEDBACK_DETAILS ";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String SubjectName = resultSet.getString("SUBJECT_NAME");
                String Feedback = resultSet.getString("FEEDBACK");               
                FeedbackDetails feedbackDetails= new FeedbackDetails(id,SubjectName,Feedback);
                FeedbackDetails.add((FeedbackDetails) FeedbackDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(FeedbackDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return FeedbackDetails;
    
    }
     
     public String updateFeedbackDetails(Integer id,String SubjectName,String Feedback){
        try{
        Connection connection = getConnection();    
        Statement statement = connection.createStatement();
        String query = "UPDATE FEEDBACK_DETAILS SET SUBJECT_NAME = '"+SubjectName+"',FEEDBACK="+Feedback+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }
     public String deleteFeedbackDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM FEEDBACK_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "DeletNewClasse failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
     
}

    private Connection getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
     
    }     

        
        
